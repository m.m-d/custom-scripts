#!/bin/bash

#===============================================================================
# This small script helps in monitoring changes in system log files.
# Using the script:
#   $ chmod +x ./searchlog.sh
#   $ ./searchlog.sh --help
# It's recommended that the local user creat the needed files named:
#   file1.txt, file2.txt, Log-Changes
# then running the script as root after that by using:
#   $ sudo ./searchlog.sh --help
#===============================================================================

# Creating an array that contains log files' locations in the system.
FindLogArray=($(find /var/log -type f -iname '*.log' 2>/dev/null))

# Function to get names and contents of the log files.
SearchLog () {
    printf "Found: %d log files.\n\n" ${#FindLogArray[@]}
    for LOG in "${FindLogArray[@]}"
    do
        echo "Path to log file: $LOG"
        printf "%s\n" $LOG | awk -F"/" '/.+\.log/ \
    	{printf "File name: %s\nRecent File Contents:\n\n",$NF}'
        tail $LOG
        echo "=================================================="
    done
}

# Function to chech if there is any difference detected after
# running the script.
CheckDiff () {
    diff file1.txt file2.txt > /dev/null
    if [ $? -eq 1 ]
    then
	DATETIME="$(date +%d/%m/%Y\-\(%T\))"
	echo "Time stamp: $DATETIME"
	echo "Difference detected:"
	diff file1.txt file2.txt
	SEPARATOR="=================================================="
	printf "%s\n\n\n" $SEPARATOR
    fi
}

# Function that uses while loop to keep the script running.
RunWhileLoop () {
    while true
    do
	SearchLog &> file1.txt
	CheckDiff >> Log-Changes
	First="`cat Log-Changes`"
	sleep 30
	SearchLog &> file2.txt
	CheckDiff >> Log-Changes
	Second="`cat Log-Changes`"
	sleep 30
	[[ $First != $Second ]] && echo "Warning! Log Files changed" \
	    && diff -c file1.txt file2.txt --color=always
    done
}

# Main script function.
RunScript () {
    SearchLog &> file1.txt
    SearchLog &> file2.txt
    RunWhileLoop
}

# Printing help message.
ShowHelp () {
    printf "Usage: ./searchlog.sh [option] \nOptions: \n \
    --keep \t Keep the script running to record any change in log files. \n \
    --list \t List logs and exit. \n \
    --help \t Print help and exit.\n"
}

# Interacting with user input.
case $1 in 
    ("--keep") RunScript ;;
    ("--list") SearchLog ;;
    ("--help") ShowHelp ;;
    (*) echo "run: [./searchlog.sh --help] for more details."
esac