#!/bin/bash

#=================================================================================
# This small script helps in downloading files completely
# To use this script you have to make it executable and copy it to ~/.config/sway
#    chmod +x download_helper.sh
#    cp download_helper.sh ~/.config/sway/
# Then add the following line to your sway config file 
#    bindsym $mod+shift+d exec $term -e $HOME/.config/sway/download_helper.sh
#=================================================================================

# Setting the current window floating and resizing it
swaymsg floating enable
swaymsg resize set 800 500

# Getting user's input
MYURL=$(dialog --title "Enter download URL" \
    --inputbox "Or leave it blank to download copied URL" \
    15 50 \
    3>&1 1>&2 2>&3 3>&-)
# 3 (new files descriptor) redirected to 1 (stdout),
# then 1 (stdout) to 2 (stderr), 
# then 2 (stderr) to 3 and deleting it.

# Reading last copied item if no url
# was entered manually
[ -z $MYURL ] && MYURL="`wl-paste`" 

# Creating some functions
# Custom directory
MyDownloadCustom () {
    clear
    CUSTOMPATH=$(dialog --title "Path to download" \
    --inputbox "Or leave it blank to download to default directory" \
    15 50 \
    3>&1 1>&2 2>&3 3>&-)
    # Cheching what app should be used
    if [[ $MYURL =~ "youtube.com" ]] 
    then
	# Using defaults or user specific PATH
	[ ! -z $CUSTOMPATH ] && cd $CUSTOMPATH || cd $HOME/Videos
	clear
	echo "Downloading YT video to $PWD"
	youtube-dl -c "$MYURL"
	while [ $? -eq 1 ]
	do
	   youtube-dl -c "$MYURL"
	done
    else
	[ ! -z $CUSTOMPATH ] && cd $CUSTOMPATH || cd $HOME/Downloads
	clear
	echo "Downloading file to $PWD..."
	axel -n 6 "$MYURL" -a
	while [ $? -eq 1 ]
	do 
	   axel -n 6 "$MYURL" -a
	done
    fi
    clear
}    

# Displaying choises for the user and checking 
# the validity of the url
RunScript () {
    # ^ start of the line, group (1st or 2nd) then a character of more
    # then "/" then a character or more ".+", $ end of the line
    if [[ $MYURL =~ ^(https?://|https?://www.).+/.+$ ]]
    then
    clear
    ANSWER=$(dialog --title "Download now?" \
	--menu "Please select one of the following" \
	15 50 3 \
	y "Yes and choose custom dir" \
	ys "Yes download then shutdown" \
	cancel "Do not download:" \
	3>&1 1>&2 2>&3 3>&-)

	case $ANSWER in
	   ([Yy]) MyDownloadCustom && echo "Download completed." ;;
	   ([Yy][Ss]) MyDownloadCustom && systemctl poweroff -i ;;
	   (*)        echo "Cancelled" ;;
	esac
    else
	clear
	echo "Enter a valid download URL"
    fi
}

# Checking required apps
[ -e /usr/bin/dialog ] || echo "Please install 'dialog' to proceed."
[ -e /usr/bin/youtube-dl ] && [ -e /usr/bin/axel ] && RunScript || echo "Please install Youtube-dl and Axel first."