#!/bin/bash
# This script will extract some system info and display it
# in sway-bar using status_command
# you can use the following emojis instead
# 🔋 🔉 🔇 🔊 📶 ☀️

StatusBar () {
    # Internet connection
    CONNECTION=$(nmcli connection show --active \
        | grep $(ip route | grep -Eo \
        "dev[[:space:]][a-zA-Z0-9]?+" \
        -m 1 | cut -d' ' -f2) 2>/dev/null | cut -d' ' -f1)
    
    [ ! -z $CONNECTION ] && [ $CONNECTION != "virbr0" ] && NETWORK="  "$CONNECTION" " \
	|| NETWORK=" Disconnected " # Wifi or antenna emoji
    
    # Brightness
    BRIGHTNESS=$(light -G | awk -F"." '{print $1"%"}') # Sun or lamp emoji
   
    # CPU and RAM
    CPU_PERC=$(mpstat | awk 'NR==4 {printf "  %.0f%\n", $4+$5+$6+$7+$8+$9}')
    RAM_PERC=$(free --mega | awk '/Mem:/ {printf "   %.0f%\n", $3/$2*100}')
    SYS_MON="$CPU_PERC"" $RAM_PERC"
    
    # Volume level
    VOLUME=$(amixer sget Master | grep -Eo -m 1 "[0-9][0-9]?[0-9]?%")
    MUTED=$(amixer sget Master | grep -Eo -m 1 "\[[o][nf]?[f]?\]")
    if [[ "$VOLUME" == "0%" ]] || [[ "$MUTED" == "[off]" ]]
    then
	SOUND="Muted" # Muted voice emoji
    elif [[ "$(echo $VOLUME | cut -d '%' -f1)" -gt "50" ]]
    then	
	SOUND="   "$VOLUME" "  # High voice emoji
    elif [[ "$(echo $VOLUME | cut -d '%' -f1)" -lt "51" ]]
    then
	SOUND="  "$VOLUME" "  # Intermediate voice emoji
    fi
    
    # Battery and charging level
    BAT_STATE=$(echo $(upower -i $(upower -e | grep 'BAT') \
	| awk '/state/ {if ($2=="charging") print "  "; \
	  else if ($2=="fully-charged") print "✔ "}'))
    BAT_PERC=$(echo $(upower -i $(upower -e | grep 'BAT') \
	| awk '/percentage/ {gsub(/%/,"",$2); print $2}'))
    if [[ "$BAT_PERC" -gt "89" ]]
    then
	BATTERY="  ""$BAT_STATE ""$BAT_PERC% " # Full battery emoji
    elif [[ "$BAT_PERC" -lt "90" ]] && [[ "$BAT_PERC" -gt "49" ]]
    then
	BATTERY="  ""$BAT_STATE ""$BAT_PERC% " # 3/4 full battery
    elif [[ "$BAT_PERC" -lt "50" ]] && [[ "$BAT_PERC" -gt "19" ]]
    then	
	BATTERY="  ""$BAT_STATE ""$BAT_PERC% " # 1/2 full
    elif [[ "$BAT_PERC" -lt "20" ]]
    then
	BATTERY="  ""$BAT_STATE ""$BAT_PERC% " # empty battery 
    fi	
    
    # Keyboard layout
    IDENTIFIER="1:1:AT_Translated_Set_2_keyboard" # Your keyboard identifier
    KB_LAYOUT=$(swaymsg -r -t get_inputs \
	| grep "$IDENTIFIER" -A15 \
	| awk -F\" '/xkb_active_layout_name/ {print $4}')
    
    # Date and time
    DATE=$(date +'%Y-%m-%d | %l:%M:%S %p')
    
    echo "$KB_LAYOUT | $NETWORK | $SOUND | $BATTERY \
|   $BRIGHTNESS | $SYS_MON | $DATE"    
}

while StatusBar
do
    sleep 1
done