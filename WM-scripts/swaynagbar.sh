#!/bin/bash
# swaynag bar to add power button to waybar
# copy this file to your waybar config directory
# and make it executible then add the following 
# lines to your waybay config file:

#     "custom/sys_mon": {
#         "format": "Power ⏻  ", 
#         "tooltip": false,
#         "on-click": "$HOME/.config/waybar/swaynagbar.sh"
#         },

swaynag -t warning -m \
    'You pressed the power button! Choose what you want to do: ' \
    -b 'Shutdown' 'systemctl poweroff' \
    -b 'Reboot' 'systemctl reboot' \
    -b 'Logout' 'swaymsg exit' \
    -b 'Lock Screen' 'swaylock -c 4e4e4e --font Roboto' \
    -b 'System monitor' 'konsole --hide-menubar --fullscreen -e "htop"' \
    --background e3e3e3 \
    --border e3e3e3 \
    --button-background e3e3e3 \
    --border-bottom 0055bb \
    --border-bottom-size 5