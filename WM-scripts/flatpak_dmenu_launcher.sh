#!/bin/bash

#===============================================================================
# This is a simple script to display and run flatpak apps in dmenu and SwayWM 
# without changing the PATH env variable. 
# To use this script you have to make it executable and copy it to ~/.config/sway
#    chmod +x flatpak_dmenu_launcher.sh
#    cp flatpak_dmenu_launcher.sh ~/.config/sway/
# Then add the following two lines to your sway config file:
#    set $flatpakmenu $HOME/.config/sway/flatpak_dmenu_launcher.sh
#    bindsym $mod+shift+m exec $flatpakmenu 
#===============================================================================

# Variables
MENUITEMS=
PUREMENUITEMS=

# Custom Paths separator
IFS=" - "

# Write all flatpak paths separated by " - " in the following variable
SCANPATHS="$HOME/bin - /var/lib/flatpak/exports/bin -
~/.local/share/flatpak/exports/share/applications - /var/lib/flatpak/exports/share/applications"

# Some sorting to create a variable containing full name used by flatpak-run command
for FILEDER in $SCANPATHS 
do
    # Only if the directory exist, the variable will be updated with all files 
    # in flatpak paths specified by user above, but if there is any error
    # it will be directed to /dev/null
    [ -d "$FILEDER" ] && MENUITEMS=$MENUITEMS$'\n'"`ls $FILEDER 2>/dev/null`"
    # Now the script will choose exutible files only and will exclude any file 
    # ending with ".desktop" then append these values to the last variable.
    if [[ ! $MENUITEMS =~ ".desktop" ]] ; then PUREMENUITEMS+=$MENUITEMS ; fi
done

# Creating a function to get user's choice and run the desired app using dedicated graphics card
StartOrNotify () {
    # Using awk to display the applications' names in a fancy look in dmenu
    # then passing the output for another variable to be used later 
    CHOSENAPP=$(echo "$PUREMENUITEMS" | awk -F "." '!/^$/ { print $NF }' | sort -u | bemenu --fn "FontAwesome 14" --hf "#0084ff" -b -p "Flatpak" -i )
    # Testing the user's choice
    if [ ! -z "$CHOSENAPP" ] 
    then
    # Passing the chosen app name back to the variable that can be used by flatpak and grepping  
    # the first occurrence if there are more than one value detected, otherwise it will send error
    # notification to the user
	echo "$PUREMENUITEMS" | grep -m 1 $CHOSENAPP | DRI_PRIME=1 xargs flatpak run -- || notify-send "Error" "Flatpak app must be specified"
    fi
}
StartOrNotify