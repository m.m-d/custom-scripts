#!/bin/bash

# A simple script to change power policies using
# cpupower tool for intel processors.

# Dont forget to edit visudo as the following:
# username ALL=NOPASSWD:/usr/bin/cpupower

# Minimal: 50% of upper limit and uses "powersave" governor.
# Medium:  70% + powersave
# Normal:  100% + powersave
# Maximal: 100% + "performance" governor.
# performance 	Run the CPU at the maximum frequency.
# powersave 	Run the CPU at the minimum frequency. 

# Getting hardware limits
MIN_LIMIT=$(cpupower frequency-info -l | awk '/^[0-9]/ {print $1}')
MAX_LIMIT=$(cpupower frequency-info -l | awk '/^[0-9]/ {print $2}')

# Getting user choice
CHOICE=$(printf "Minimal\nMedium\nNormal\nMaximal" |\
    dmenu -fn "Fira Code 14" -l 5 -i -p "Preferred performance:")
    # You can use bemenu instead of dmenu in wayland.

case $CHOICE in

  Minimal)
    DESIRED_VAL=$(expr $(cpupower frequency-info -l |\
        awk '/^[0-9]/ {print $2}') \* 50 / 100)
    if [ $DESIRED_VAL -gt $MIN_LIMIT ] ; then
        sudo cpupower -c all frequency-set \
            -g powersave --max=$DESIRED_VAL
        notify-send "Preferred power mode" "Minimal"
    else
        notify-send "Error"
    fi
    ;;

  Medium)
    DESIRED_VAL=$(expr $(cpupower frequency-info -l |\
        awk '/^[0-9]/ {print $2}') \* 70 / 100)
    if [ $DESIRED_VAL -gt $MIN_LIMIT ] ; then
        sudo cpupower -c all frequency-set \
            -g powersave --max=$DESIRED_VAL
        notify-send "Preferred power mode" "Medium"
    else
        notify-send "Error"
    fi
    ;;

  Normal)
    sudo cpupower -c all frequency-set -g powersave --max=$MAX_LIMIT --min=$MIN_LIMIT
    notify-send "Preferred power mode" "Normal"
    ;;

  Maximal)
    sudo cpupower -c all frequency-set -g performance --max=$MAX_LIMIT --min=$MIN_LIMIT
    notify-send "Preferred power mode" "Maximal"
    ;;

  *)
    notify-send "Preferred power mode" "No change"
    ;;
    
esac
