#!/bin/bash

### Part one ###

# (please see the example file included is this project)
# This is an awk script that raeds contents.txt
# file and prints lines that doesn't start 
# with "+" sign, then 
# extracts the time stamp between ()
# then removes "(" and ")"
# and passes the output to another awk command 
# to calculate the time in seconds.

# This will be used later to run specific section of
# a video in vlc.

# How to run:
# $ chmod +x awk-to-vlc.sh
# $ chmod +x file-to-awk.sh
# to see sections of the file extracted by this part:
# $ ./file-to-awk.sh
# to run the script use the command:
# $ ./awk-to-vlc.sh 
# then enter the section number and file name, then
# it will be displayed in vlc.
# for more info please read the comments in the second part.



awk '/\(.+/ { if ($1 =! /^\+/) print $NF }' contents.txt \
    | awk -F":" ' \
    BEGIN { printf "%s\t%s \n\n","Next section", "Time in seconds" } \
    { gsub(/\(/,"",$1); \
    gsub(/\)/,"",$NF); \
	TIME_IN_s=3600*$1+60*$2+$3 ;
	printf "%d\t\t%d\n",NR, TIME_IN_s \
    }'