#!/bin/bash

## Part two ##

# This is the second part of the awk to vlc script
# it will extract the time in seconds calculated by the first
# part and will display it using vlc from a specific video.

# How to run:
# $ chmod +x awk-to-vlc.sh
# $ chmod +x file-to-awk.sh
# to see sections of the file extracted by this part:
# $ ./file-to-awk.sh
# to run the script use the command:
# $ ./awk-to-vlc.sh "video name.format" 
# then enter the section number and file name, then
# it will be displayed in vlc.
# for more info please read the comments in the first part.

VIDNAME=$1

echo "Please choose what section you want to run
[ or press return to run first section to be viewed ]:"
read SECTION
[ -z $SECTION ] && SECTION=1

printf "File name: \n$VIDNAME \nSection no: $SECTION \n\n"

./file-to-awk.sh | \
    vlc "$VIDNAME" --start-time="$(awk -v section=$SECTION \
    '{ if ($1 == section) print $2 }')" 2>/dev/null